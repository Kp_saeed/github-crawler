$(function(){
  
  // Using local storage for cache mechanism
  let cache = window.localStorage;

  // Do the main function when the button submitted
  $('#myForm').on('submit', function(e){
    e.preventDefault();
    
    // Get username from form
    var username = $('#ghusername').val();

    // Generate urls which we need to communicate with Github API
    var user_url   = 'https://api.github.com/users/' + username;
    var events    = 'https://api.github.com/users/' + username + '/events?per_page=100';
    var repositories = 'https://api.github.com/users/' + username + '/repos?per_page=100';

    // Check whether the user's information exists in local storage or not
    if(cache.getItem(username)){
      console.log("Getting From Cache.");

      // Getting user's information from local storage
      var outhtml = get_from_cache(username);

      // show information in the html page
      $('#ghapidata').html(outhtml);
    }

    else{

      // Clear local storage when searching for a new user
      cache.clear();

      // Get user information from Github API
      requestJSON(user_url, function(json) {
        console.log("Getting From API.");

        //Showing error if there is no user with the given username or no username provided
        if(json.message == "Not Found" || username == '') {
          $('#ghapidata').html('<span class="error-msg">' + 'There is no user with the specified username...' + '</span>');
      }
      
        else {

          // Storing the information we need in variables
          var fullname   = json.name;
          var aviurl     = json.avatar_url;
          var weblog     = json.blog;
          var location   = json.location;
          var biography  = json.bio;

          if(fullname == undefined) {
            fullname = username; 
          }

          // Store information in local storage for the specified user
          set_in_cache(username, fullname, aviurl, weblog, location, biography);

          // Generate HTML output page
          var outhtml = make_output(fullname, aviurl, weblog, location, biography); 
        
          // Get Favourite Language of user (Two different methods implemented.)
          get_favourite_lang(repositories);
          // get_favourite_lang_with_events(events);
        
          // show information in the html page
          $('#ghapidata').html(outhtml);                         

          // Get user's favourite language by searching last 5 repositories of user
          function get_favourite_lang(repositories){
            let j = 0;
            const langs = [];

            $.get(repositories, function(data){ 
              for(let i = 0; i < data.length; i++){
                if(data[i].language != null && j < 5){
                  langs.push(data[i].language);
                  j++;
                }
              }
              const fav =  favourite_lang(langs);
              cache.setItem(`${username}_fav`, fav);

              if(fav != null){
                $('.lang-sec').html('<span>' + 'Favourite language: ' + fav + '</span>');
              }
              else{
                $('.lang-sec').html('<span>' + 'No Favourite language' + '</span>');
              }
            });
          }

          // Get user's favourite language by searching last 5 repositories that user pushed in them
          function get_favourite_lang_with_events(events){
            $.get(events, function(data){
              let j = 0;
              let repository = [];
              for (let i = 0; i < data.length; i++) {
                if( data[i].type == 'PushEvent'){
                  repository.push(data[i].repo);
                }
              }
            
              const urls = new Set();
              for (let i = 0; i < repository.length; i++) {
                if(urls.size < 5){
                  urls.add(repository[i].url);
                }
              }
            
              let langs = [];
              for (let item of urls.values()){
                $.get(item, function(urlData){
                  $.get(urlData.languages_url, function(langData){
                    for (var languages in langData){
                      langs.push(languages);
                    }
                  });
                });
              }
              const fav = favourite_lang(langs); 
              if(fav != null){
                $('.lang-sec').html('<span>' + 'Favourite language: ' + fav + '</span>');
              }
              else{
                $('.lang-sec').html('<span>' + 'No Favourite language' + '</span>');
              }
            });
          }
      
          //Get the most frequented language in the list of user's languages
          function favourite_lang(array){
            if(array.length == 0)
              return null;
            
            var modeMap = {};
            var maxEl = array[0], maxCount = 1;

            for(var i = 0; i < array.length; i++){
              var el = array[i];
              if(modeMap[el] == null)
                modeMap[el] = 1;
              else
                modeMap[el]++;  
              if(modeMap[el] > maxCount){
                maxEl = el;
                maxCount = modeMap[el];
              }
            }
            return maxEl;
          }

          // Store information in local storage
          function set_in_cache(username, fullname, aviurl, weblog, location, biography){
            cache.setItem(username, username);
            cache.setItem(`${username}_fullname`, fullname);
            cache.setItem(`${username}_avatar`, aviurl);
            cache.setItem(`${username}_web`, weblog);
            cache.setItem(`${username}_loc`, location);
            cache.setItem(`${username}_biog`, biography);
          }

          // Generate HTML content to show on page when not using local storage
          function make_output(fullname, aviurl, weblog, location, biography){
            var outhtml = '<span class="fullname">' + "NAME: " + fullname + '</span>';
            outhtml = outhtml + '<div class="ghcontent"><img src="' + aviurl + '" width="100" height="100" alt="'+'"></div>';

            if(weblog != null && weblog != ""){
              outhtml = '<br>' + outhtml + '<span class="blog-sec">' + "BLOG: " + weblog + '</span>' + '<br>';
            }
            else{
              outhtml = '<br>' + outhtml + '<span class="blog-sec">' + "BLOG: " + "No Blog" + '</span>' + '<br>';
            }

            if(location != null && location != ""){
              outhtml = outhtml + '<span class="location-sec">' + "LOCATION: " + location + '</span>' + '<br>';
            }
            else{
              outhtml = outhtml + '<span class="location-sec">' + "LOCATION: " + "No Location" + '</span>' + '<br>';
            }

            if(biography != null && biography != ""){
              outhtml = outhtml + '<span class="bio-sec">' + "BIOGRAPHY: "  + biography + '</span>';
            }
            else{
              outhtml = outhtml + '<span class="bio-sec">' + "BIOGRAPHY: "  + "No Biography" + '</span>';
            }

            outhtml = outhtml + '<span class="lang-sec">' + '</span>';

            return outhtml;
          }
        }
      });
    }
  });

  // Generate HTML content to show on page when using local storage
  function get_from_cache(username){
    var outhtml = '<span class="fullname">' + "NAME: " +  cache.getItem(`${username}_fullname`) + '</span>';
    outhtml = outhtml + '<div class="ghcontent"><img src="' + cache.getItem(`${username}_avatar`) + '" width="100" height="100" alt="'+'"></div>';
    
    if(cache.getItem(`${username}_web`) != null && cache.getItem(`${username}_web`) != ""){
      outhtml = '<br>' + outhtml + '<span class="blog-sec">' + "BLOG: " + cache.getItem(`${username}_web`) + '</span>' + '<br>';
    }
    else{
      outhtml = '<br>' + outhtml + '<span class="blog-sec">' + "BLOG: " + "No Blog" + '</span>' + '<br>';
    }
    
    if(cache.getItem(`${username}_loc`) != "null" && cache.getItem(`${username}_loc`) != ""){
      outhtml = outhtml + '<span class="location-sec">' + "LOCATION: " + cache.getItem(`${username}_loc`) + '</span>' + '<br>';
    }
    else{
      outhtml = outhtml + '<span class="location-sec">' + "LOCATION: " + "No Location" + '</span>' + '<br>';
    }

    if(cache.getItem(`${username}_biog`) != "null" && cache.getItem(`${username}_biog`) != ""){
      outhtml = outhtml + '<span class="bio-sec">' + "BIOGRAPHY: " + cache.getItem(`${username}_biog`) + '</span>';
    }
    else{
      outhtml = outhtml + '<span class="bio-sec">' + "BIOGRAPHY: " + "No Biography" + '</span>';
    }

    if(cache.getItem(`${username}_fav`) != "null" && cache.getItem(`${username}_fav`) != ""){
      outhtml = outhtml + '<span class="lang-sec">'+ 'Favourite language: ' + cache.getItem(`${username}_fav`) + '</span>';
    }
    else{
      outhtml = outhtml + '<span class="lang-sec">' + "No Favourite language" + '</span>';
    }

    outhtml = outhtml + '<div class="ghcontent1"><span class="desc">' + 'Items are retrieved from local storage.' + '</span></div>';

    return outhtml;
  }
  
  // Getting the url content in JSON format
  function requestJSON(url, callback) {
    $.ajax({
      url: url,
      complete: function(xhr) {
        callback.call(null, xhr.responseJSON);
      }
    });
  }
});
  